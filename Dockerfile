FROM rocshers/python-poetry:3.8-slim-1.2.2 as base

WORKDIR /container

RUN apt update -y && \
    apt install -y git && \
    rm -rf /var/lib/{cache,log}/ && \
    rm -rf /var/lib/apt/lists/* && \
    rm -r /var/log

RUN poetry self add poetry-git-version-plugin

COPY ./pyproject.toml ./poetry.lock ./

RUN poetry --no-plugins install --no-root && \
    poetry --no-plugins cache clear --all -n .

COPY ./ ./
