from typing import Dict
from unittest.mock import MagicMock

import pytest

from poetry_git_version_plugin.config import PLUGIN_NAME, PluginConfig
from poetry_git_version_plugin.services import VersionService


@pytest.fixture()
def version_service_config():
    return {
        'ignore_pep440': False,
        'ignore_public_pep440': False,
        'ignore_semver': True,
        'ignore_errors': False,
    }


@pytest.fixture
def git_version_service(version_service_config: Dict[str, str]):
    class PyProject(object):
        data: Dict[str, dict]

        def __init__(self) -> None:
            self.data = {'tool': {PLUGIN_NAME: version_service_config}}

    config = PluginConfig(PyProject())  # type: ignore

    return VersionService(MagicMock(spec=None), config)
