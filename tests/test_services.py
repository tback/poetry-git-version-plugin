import pytest

from poetry_git_version_plugin.exceptions import (
    InvalidCanonPepVersionException,
    InvalidPepVersionException,
    InvalidVersionException,
    PluginException,
)
from poetry_git_version_plugin.services import VersionService, validate_version


class TestValidateVersion(object):
    @pytest.mark.parametrize(
        'version',
        [
            '1.3.2',
        ],
    )
    def test_valid_public_pep440(self, version: str):
        validate_version(version)

    @pytest.mark.parametrize(
        'version',
        [
            '1.3.2a1',
            '1.3.2b1',
        ],
    )
    def test_valid_semver(self, version: str):
        with pytest.raises(InvalidVersionException):
            assert validate_version(version)

    @pytest.mark.parametrize(
        'version',
        [
            'v1.3.2',
            '1.3.2a',
            '1.3.2b',
            '1.3.2+5',
            '1.3.2+5-5babef6',
            '1.3.2+5-5babef6',
            '1.3.2.a5+5babef6',
            '1.3.2+a5-5babef6',
        ],
    )
    def test_valid_pep440(self, version: str):
        with pytest.raises(InvalidCanonPepVersionException):
            assert validate_version(version)

    @pytest.mark.parametrize(
        'version',
        [
            '1.2.3.qwerty',
            'qwerty',
            'qwerty0.0.0',
            '0.0.0+7+7',
        ],
    )
    def test_fail(self, version):
        with pytest.raises(InvalidPepVersionException):
            assert validate_version(version)


class TestVersionService(object):
    def test_init(self, git_version_service) -> None:
        assert isinstance(git_version_service, VersionService)

    @pytest.mark.parametrize(
        'version',
        [
            'v1.3.2',
            '1.3.2a',
            '1.2.3+qwerty',
            '1.2.3.qwerty',
        ],
    )
    def test_error_validate_version(self, git_version_service: VersionService, version: str) -> None:
        with pytest.raises(PluginException):
            git_version_service.validate_version(version)

    def test_get_version(self, git_version_service: VersionService) -> None:
        assert git_version_service.get_version() is not None
