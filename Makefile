ifeq ($(OS), Windows_NT)

define log
	@echo ">>> $(1)"
endef

else

define log
	@tput bold 2>/dev/null || exit 0
	@tput setaf 3  2>/dev/null || exit 0
	@echo ">>> $(1)"
	@tput sgr0  2>/dev/null || exit 0
endef

endif


# Секции python команд


.PHONY: install
install:
	$(call log, installing packages)
	poetry install --no-dev


.PHONY: install-dev
install-dev:
	$(call log, installing development packages)
	poetry install
	poetry run pre-commit install


.PHONY: format
format:
	$(call log, reorganizing imports & formatting code)
	poetry run black .
	poetry run isort .
	poetry run ruff . --fix --exit-zero 
	poetry run pre-commit run --all-files
	poetry run mypy .


.PHONY: test
test:
	$(call log, running tests)
	poetry run pytest --cov=. --cov-report term:skip-covered --cov-report=html .
	

.PHONY: profiling
profiling:
	$(call log, running profiling)
	poetry run python -m cProfile -o profile -m pytest -l
	poetry run gprof2dot -f pstats profile | dot -Tpng -o profile.png
